# [Nom du projet]

[Brève description du projet]

## Sommaire

1. [Informations utiles](#info)
    1. [Classes conseillées](#classes)
    2. [Concepts requis](#concepts)
2. [À propos](#apropos)
    1. [Comment ça marche ?](#comment)
    2. [Pour qui est-ce que cela a été conçu ?](#qui)
3. [Utilisation dans vos projets](#utilisation)
    1. [Pré-requis](#prerequis)
    2. [Installation](#installation)
    3. [Utilisation](#utilisation)
4. [Crédits](#credits)
5. [Alternatives](#alternatives)
6. [Mainteneurs](#mainteneurs)

## Informations utiles <a name=info></a>

### Classes conseillées <a name=classes></a>

+ Année minimum :
+ Domaine Professionnel :
+ Cycle fait :

### Concepts requis <a name=concepts></a>

[Liste des concepts nécessaire à la compréhension du code]

## À propos de [Nom du projet] <a name=apropos></a>

### Comment ça marche ? <a name=comment></a>

[Description du fonctionnement du code]

### Pour qui est-ce que cela a été conçu ? <a name=qui></a>

## Utilisation de [Nom du projet] dans vos projets <a name=utilisation></a>

### Pré-requis <a name=prerequis></a>

[Mettre ici les programmes nécessaire au fonctionnement de votre projet, s'il y en a]

### Installation <a name=installation></a>

[Mettre un lien vers la dernière version du code et les explications pour l'installation]

### Utilisation <a name=utilisation></a>

[Mettre un lien vers le manuel d'utilisation ou, s'il n'y en a pas, mettre les instructions d'utilisation]

## Crédits <a name=credits></a>

[Liste des librairies utilisées, s'il y en a]

## Alternatives <a name=alternatives></a>

[Liste des projets ayant le même but que le votre]

## Mainteneurs <a name=mainteneurs></a>

[Liste des mainteneurs du projet]