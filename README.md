# Administration

Ce projet est là pour répondre aux questions concernant l'ITOO et son fonctionnement.

Il contient également toutes les ressources utilisables pour développer vos projets qui ne sont pas du code.

Visitez le [wiki](https://framagit.org/ISENToulonOpenOrganization/administration/wikis/home) pour en savoir plus.