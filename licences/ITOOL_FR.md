# ISEN Toulon Open Organization Licence (ITOOL)

Version 1, 22 Juin 2019

Copyright &copy; 2019 ISEN Toulon Open Organization

## Préambule

L'utilisation de cette licence est limité aux projets présents sur le groupe git [ISEN Open Organization](https://framagit.org/ISENToulonOpenOrganization), hébergé par [Framagit](framagit.org).  


## 1. Définitions

### 1.1 Définitions Générales

1. `"ITOO"` fait référence à l'ISEN Toulon Open Organization, le groupe git présent sur Framagit.
2. `"La licence"` fait référence à l'ISEN Toulon Open Organization Licence, matérialisée par ce document
3. `"Ce projet"` fait référence à tous les fichiers contenus dans le même dossier que la licence ainsi que de tous les fichiers contenus dans ses sous-dossiers, à l'exception du sous-dossier `"lib"`. Les fichiers contenus dans ce dernier sont sous les règles des licences qui leurs sont associés.
4. `"Code source"` fait référence au contenu des fichiers constituant le projet.

### 1.2 Actions

1. `"Lecture"` : Action d'ouvrir les fichiers de ce projet afin d'en étudier le code source.
2. `"Contribution"` : Action de modifier le code source de ce projet.

### 1.3 Statuts des projets

1. `"Projet personnel"` : Projet ne disposant que d'un seul contributeur et n'ayant pas vocation à être distribué.
2. `"Projet collectif"` : Projet disposant d'une équipe d'un ou plusieurs contributeurs et ayant pour vocation d'être distribué.

### 1.4 Statuts des personnes

1. `"Externe"` : Personne n'ayant jamais possédé d'adresse mail `"@isen.yncrea.fr"`, `"@yncrea.fr"` ou `"@isen.fr"`.
2. `"Interne"` : Personne étudiant ou travaillant à l'ISEN Toulon et possédant de ce fait une adresse mail `"@isen.yncrea.fr"`, `"@yncrea.fr"` ou `"@isen.fr"`.
3. `"Contributeur"` : Personne Interne membre de l'ITOO ayant apporté des contributions à ce projet.

## 2. Permissions

### 2.1 Lecture du code source de ce projet

La lecture du code source est autorisée pour les personnes externes, internes et pour les contributeurs de ce projet.

### 2.2 Modification du code source de ce projet

1. Pour les personnes externes, les modifications du code source de ce projet sont autorisées de manière locale : elle ne peuvent pas être mises en ligne sur l'ITOO.
2. Pour les personnes internes et les contributeurs, les modifications du code source de ce projet sont autorisées de manière locale et globale : elles peuvent être mise en ligne sur l'ITOO.

### 2.3 Réutilisation du code source de ce projet

#### 2.3.1 Projet personnel

La réutilisation du code source de ce projet est autorisée sans condition pour les personnes externes, internes et pour les contributeur de ce projet.

#### 2.3.2 Projet dans le cadre de l'ISEN

Pour une personne interne, la réutilisation du code source de ce projet est autorisée dans le cadre d'un projet de l'ISEN à condition qu'il respecte les clauses suivantes :

1. L'utilisation de code exterieur est autorisé par le professeur référent.
2. Les modifications faites au code source de ce projet doivent être publié et accessibles en lecture par tout le monde.
3. Ce projet doit être cité dans la documentation du nouveau projet et lors des différentes présentations de ce dernier.

#### 2.3.3 Projet exterieur à l'ISEN

1. Pour une personne externe, la réutilisation du code source de ce projet est autorisée dans le cadre d'un projet collectif à condition qu'il respecte les clauses suivantes :
    1. Le nouveau projet doit être à but non lucratif.
    2. La licence sous laquelle est publié le nouveau projet doit être une licence libre.
    3. Ce projet doit être cité dans la documentation du nouveau projet.
2. Pour une personne interne, la réutilisation du code source de ce projet est autorisée dans le cadre d'un projet collectif à condition qu'il respecte les clauses suivantes :
    1. Les modifications faites au code source de ce projet doivent être accessibles en lecture par tout le monde.
    2. Ce projet doit être cité dans la documentation du nouveau projet.
3. Pour un contributeur de ce projet, la réutilisation du code source de ce projet est autorisée dans le cadre d'un projet personnel ou collectif à la condition suivante :
    1. L'application de règles juridiques sur le nouveau projet ne doit pas avoir de conséquence, directe ou indirecte, sur l'application des règles de cette licence sur ce projet.

## 3. Auteurs

+ [Luca Mayer-Dalverny](https://framagit.org/LucaMayerDalverny)