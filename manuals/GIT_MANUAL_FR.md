# Manuel d'utilisation de Git

Auteur : Luca Mayer-Dalverny

## Sommaire

1. [Qu'est-ce que Git ?](#quoi)
2. [Pour commencer](#debut)
    1. [Installation](#installation)
        1. [Linux](#linux)
        2. [MacOS](#mac)
        3. [Windows](#windows)
    2. [Configuration](#config)
3. [Création d'un projet](#creation)
    1. [Création à partir de zéro](#creation-zero)
    2. [Création depuis un dossier existant](#creation-dossier)
    3. [Clone depuis un projet en ligne](#creation-clone)
    4. [Le fichier .gitignore](#creation-gitignore)
4. [Modification d'un projet](#modification)
    1. [Voir les modifications du projet](#modification-voir)
    2. [Ajouter et supprimer des fichier du git](#modification-as)
    3. [Commit](#modification-commit)
    4. [Pull](#modification-pull)
    5. [Push](#modification-push)
5. [Les branches](#branches)
    1. [Pourquoi utiliser des branches ?](#branches-pourquoi)
    2. [Créer une nouvelle branche](#branches-creation)
    3. [Changer de branche](#branches-change)
    4. [Fusionner des branches](#branches-fusion)
    5. [Supprimer des branches](#branches-supprimer)
6. [Les tags](#tags)
    1. [Pourquoi utiliser les tags ?](#tags-pourquoi)
    2. [Créer un tag](#tags-creation)
    3. [Push](#tags-push)

## Qu'est-ce que Git ? <a name="quoi"></a>

Git est un système de gestion de version créé par Linus Torval en 2005. Il est utilisé pour suivre les changements effectués sur les fichiers d'un ordinateur ainsi que pour coordonner le travail de plusieurs personnes sur ces même fichiers. Il est principalement utilisé dans le cadre de la gestion de code source dans le développement logiciel.

## Pour commencer <a name="debut"></a>

### Installation <a name="installation"></a>

#### Linux <a name="linux"></a>

Pour les distribution Debian/Ubuntu, tapez la commande suivante dans un terminal :

```bash
apt-get install git
```

Pour les autres distributions, vous pouvez trouver la commande correspondante [ici](https://git-scm.com/download/linux).

#### MacOS <a name="mac"></a>

La version de Git pour MacOS peut être téléchargé [ici](https://git-scm.com/download/mac).

#### Windows <a name="windows"></a>

La version de Git pour Windows peut être téléchargé [ici](https://git-scm.com/download/win)

### Configuration <a name="config"></a>

Une fois que Git est installé sur votre ordinateur, vous devez configurer votre identité avec votre nom et votre adresse mail, afin de signer les modifications que vous effectuerez sur vos projet avec Git.  Pour ce faire, tapez les commandes suivante dans un terminal :

```bash
git config --global user.name "Votre nom"
git config --global user.email "Votre adresse mail"
```

## Création d'un projet <a name="creation"></a>

Pour créer un projet avec Git, plusieurs options s'offrent à vous :

+ Créer votre projet depuis un nouveau dossier
+ Créer votre projet depuis un dossier existant contenant déjà des fichiers
+ Créer votre projet en ligne depuis un service web d'hébergement utilisant Git (comme [Framagit](https://framagit.org/), [Gitlab](https://gitlab.com/) ou [Github](https://github.com/)).

### Création à partir de zéro <a name="creation-zero"></a>

```bash
# Création du dossier contenant votre projet
mkdir mon-projet
# Changement du repertoire courant vers le dossier créé
cd mon-projet
# Création du projet Git
git init
```

### Création depuis un dossier existant <a name="creation-dossier"></a>

```bash
# Changement du repertoire courant vers le dossier de votre projet
cd mon-projet
# Création du projet Git
git init
```

### Clone depuis un projet en ligne <a name="creation-clone"></a>

Après avoir créé votre projet en ligne, tapez la commande suivante dans un terminal :

```bash
git clone url-de-votre-projet
```

La commande suivante va créer un nouveau dossier portant le nom de votre projet et y télécharger tous les fichiers de votre projet en ligne.

### Le fichier .gitignore <a name="creation-gitignore"></a>

S'il y a des fichiers dans le dossier de votre projet que vous ne voulez pas ajouter au projet Git, comme les fichiers de configuration de votre environement de développement, vous pouvez créer un fichier `.gitignore`. Ce fichier contient les répertoires des fichiers que vous ne voulez pas avoir dans votre projet Git.  
Voici un exemple de fichier .gitignore :

```bash
# Ceci est un commentaire

# Tous les fichier ayant l'extension .o seront ignorés par Git
*.o
# À l'exception du fichier main.o situé dans le dossier obj/
!/obj/main.o

# Le fichier data.txt dans le dossier ressources/ sera ignoré par Git
/ressources/data.txt
```

## Modification d'un projet <a name="modification"></a>

### Voir les modifications du projet <a name="modification-voir"></a>

Après avoir travaillé dur votre projet, vous pouvez voir le nom des fichier que vous avez modifiés avec la commande `status` :

```bash
git status
```

Pour avoir plus de détails concernant les modifications d'un fichier en particulier, vous pouvez utiliser la commande `diff` :

```bash
git diff répertoire/nom-du-fichier
```

### Ajouter et supprimer des fichier du git <a name="modification-as"></a>

Quand vous avez ajouté ou modifié un fichier dans le dossier de votre projet, vous pouvez l'ajouter au Git afin que les modifications du  projet soient enregistrées.  
Pour ce faire, il faut utiliser la commande `add` :

```bash
git add répertoire/nom-du-fichier
```

Si vous avez supprimé un fichier de votre projet, il faut utiliser la commande `rm` :

```bash
git rm répertoire/nom-du-fichier
```

### Commit <a name="modification-commit"></a>

Quand les fichiers créés, modifiés ou supprimés ont été ajouté au Git, vous devez créer un `commit` afin d'enregistrer et d'expliquer les modifications faites.

```bash
git commit -m "Le message d'explication des modifications"
```

### Pull <a name="modification-pull"></a>

Si vous travaillez sur un dépôt en ligne, vous devrez faire un `pull` après vos commits. Le pull va récupérer les modifications faites au dépôt en ligne par vos co-équipier et les appliquer à vos fichiers locaux.

```bash
git pull
```

### Push <a name="modification-push"></a>

Juste après avoir fait un pull, vient le `push`. Le push va appliquer vos modifications locales enregistrées via des commit au dépôt en ligne. **Un push doit toujours être précédé d'un pull**.

```bash
git push
```

