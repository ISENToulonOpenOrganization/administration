# Git Manual

Author : Luca Mayer-Dalverny

## Table of contents

1. [What is Git ?](#what)
2. [Getting started](#start)
    1. [Setup](#setup)
    2. [Creation of your project](#creation)
        1. [Creation from your computer](#computer)
        2. [Cloning an existing project](#clone)
    3. [The .gitignore file](#gitignore)
3. [Modify your project](#modification)
    1. [See the files you changed in your project](#status)
    2. [Adding and deleting files to the git](#add)
    3. [Commiting files](#commit)
    4. [Pull request](#pull)
    5. [Push request](#push)
4. [Branches](#branch)

## What is Git ? <a name="what"></a>

Git is a version control system created by Linus Torvald in 2005. It is used to track changes in computer files as well as coordinating work on those files by multiple people. It is mainly used for source code managment in software development.

## Getting started <a name="start"></a>

### Setup <a name="setup"></a>

Once Git is installed on your computer, you have to set up your identity with your name and your email address. The modifications you make to your project will be made with the name you have given.  
To do so, write the following lines in a command prompt :

```bash
git config --global user.name "Your name"
git config --global user.email "your.name@email.com"
```

### Creation of your project <a name=creation></a>

In order to create your new project, two options are available to you : 

+ Create your project from your computer and work on it locally
+ Create your project from the hosting service and clone it on your computer

#### Creation from your computer <a name="computer"></a>

```bash
# Creation of the folder containing your project
mkdir my-project-folder
# Moving the current directory to the one of your project
cd my-project-folder
# Creation of the git repository
git init
```

#### Cloning an existing project <a name="clone"></a>

Open a command prompt in the directory where you want your project folder to be and type the following line : 

```bash
git clone https://hostgit.org/name/project.git
```

This command will create a folder named "project" and download in it every file of the repository.

### The .gitignore file <a name="gitignore"></a>

If there are files in your project folder that you don't want to put in your git repository, you have to create the .gitignore file. This file contains the paths of the files you don't want to put in your repository. You can also add comments in it, as in the following example :

```bash
# This is a .gitignore comment
# All the .o files will be ignored by git
/*.o

# The file data.txt in the folder named 'subfolder' will be ignored by git
/subfolder/data.txt
```

## Modify your project <a name="modification"></a>

### See the files you changed in your project <a name="status"></a>

When you have modified a lot of files and you don't remember which one you have modified, you can see the changes you have made with the `status` command :

```bash
git status
```

It will show you the files present on the Git that have been modified locally, the files not present on the Git you created on your computer and the files present on the Git you deleted on your computer.

### Adding and deleting files to the git <a name=add></a>

When a file has been modified or created on your computer, you have to add it to the git using the `add` command :

```bash
git add path/myfile.ext
```

If you have deleted a file loccaly, you have to use the `rm` command :

```bash
git rm path/myfile.ext
```

### Commiting files <a name="commit"></a>

Once you have added your files to the git, you have to create a commit to explain the changes you made to the files. To do so, you have to use the `commit` command :

```bash
git commit -m "Your explanation message on your changes"
```

### Pull Request <a name="pull"></a>

When all your modified and created files have been added and commited to the git, you have to do a pull request in order to get the commit made by your teammates, if there are so. The `pull` command can be used to send the request :

```bash
git pull
```

### Push Request <a name="push"></a>

Right after the pull comes the push. The push will send the commits you created to the online repository. **A push must always be preceded by a pull** and the command to do it is `push` :

```bash
git push
```

## Branches <a name="branch"></a>

The section has not been writen yet.